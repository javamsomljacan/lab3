package org.vub.lab3;

public class Policija extends Auto {
    public int brojZlocinaca;

    public Policija(int brojKonja, String model,int brojZlocinaca){
        super(brojKonja, model);
        this.brojZlocinaca=brojZlocinaca;
    }

    public void info(){
        System.out.println(model + "," + brojKonja + " konja" + "," + brojZlocinaca + " zlocinaca uhvacenih");
    }

    public void pustiZlocinca(){
        brojZlocinaca--;
    }
}
