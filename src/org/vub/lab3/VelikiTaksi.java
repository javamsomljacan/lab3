package org.vub.lab3;

public class VelikiTaksi extends Taksi {
    public int brojDodatnihPutnika;

    public VelikiTaksi(int brojKonja, String model, int brojPutnika, int brojDodatnihPutnika){
        super(brojKonja,model,brojPutnika);
        this.brojDodatnihPutnika = brojDodatnihPutnika;
    }

    public void info(){
        System.out.println(model + "," + brojKonja + " konja" + "," + brojPutnika + " prevezenih putnika" + ", " + brojDodatnihPutnika + " dodatnih ljudi prevezeno");
    }

    @Override
    public void izbaciPutnika(){
        brojPutnika = brojPutnika-2;
    }
}
