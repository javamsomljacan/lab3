package org.vub.lab3;

public class Main {

    public static void main(String[] args) {
        var auto = new Auto(200,"BMW");
        var policija = new Policija(100,"Ford", 2);
        var taksi = new Taksi(120,"Dacia",3);
        var vatrogasci = new Vatrogasci(220, "Jeep", 10, 10);
        var velikiTaksi = new VelikiTaksi(150, "Hummer", 4, 3);

        auto.info();
        policija.info();
        taksi.info();
        vatrogasci.info();
        policija.pustiZlocinca();
        policija.info();
        velikiTaksi.info();
        velikiTaksi.izbaciPutnika();
        velikiTaksi.info();
        taksi.izbaciPutnika();
        taksi.info();
    }
}
