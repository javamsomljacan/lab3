package org.vub.lab3;

public class Auto {
    public int brojKonja;
    public String model;

    public Auto(int brojKonja, String model){
        this.brojKonja=brojKonja;
        this.model=model;
    }

    public void info(){
        System.out.println(model + "," + brojKonja + " konja");
    }

}
