package org.vub.lab3;

public class Taksi extends Auto{
    public int brojPutnika;

    public Taksi(int brojKonja, String model, int brojPutnika){
        super(brojKonja,model);
        this.brojPutnika=brojPutnika;
    }

    public void info(){
        System.out.println(model + "," + brojKonja + " konja" + "," + brojPutnika + " prevezenih putnika");
    }

    public void izbaciPutnika(){
        brojPutnika--;
    }
}
