package org.vub.lab3;

public class Vatrogasci extends Auto {
    public int maxBrojOsoba;
    public int brojProtupozarnihAparata;

    public Vatrogasci(int brojKonja, String model,int maxBrojOsoba, int brojProtupozarnihAparata){
        super(brojKonja,model);
        this.maxBrojOsoba=maxBrojOsoba;
        this.brojProtupozarnihAparata=brojProtupozarnihAparata;
    }

    public void info(){
        System.out.println(model + "," + brojKonja + " konja" + "," + maxBrojOsoba + " dostupnih vatrogasaca" + ", " + brojProtupozarnihAparata + " protupozarnih aparata");
    }
}
